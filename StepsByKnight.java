//{ Driver Code Starts
import java.util.*;
import java.lang.*;
import java.io.*;
class GFG
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int T = Integer.parseInt(br.readLine().trim());
        while(T-->0)
        {
            int N = Integer.parseInt(br.readLine().trim());
            String[] S1 = br.readLine().trim().split(" ");
            String[] S2 = br.readLine().trim().split(" ");
            int[] KnightPos = new int[2];
            int[] TargetPos = new int[2];
            for(int i = 0; i < 2; i++){
                KnightPos[i] = Integer.parseInt(S1[i]);
                TargetPos[i] = Integer.parseInt(S2[i]);
            }
            Solution obj = new Solution();
            int ans = obj.minStepToReachTarget(KnightPos, TargetPos, N);
            System.out.println(ans);
       }
    }
}

// } Driver Code Ends


class Solution {
    // Function to find out minimum steps Knight needs to reach target position.
    public int minStepToReachTarget(int KnightPos[], int TargetPos[], int N) {
        // array to keep track of visited positions
        boolean[][] visited = new boolean[N + 1][N + 1];
        
        // queue to perform BFS
        Queue<int[]> queue = new LinkedList<>();
        
        // directions the knight can move
        int[][] directions = {{-2, -1}, {-2, 1}, {-1, -2}, {-1, 2}, {1, -2}, {1, 2}, {2, -1}, {2, 1}};
        
        // add initial position of knight to queue and mark it as visited
        queue.offer(new int[]{KnightPos[0], KnightPos[1]});
        visited[KnightPos[0]][KnightPos[1]] = true;
        
        // variable to store minimum steps
        int steps = 0;
        
        // perform BFS
        while (!queue.isEmpty()) {
            int size = queue.size();
            // iterate through all positions in current level
            for (int i = 0; i < size; i++) {
                int[] currentPosition = queue.poll();
                int currentX = currentPosition[0];
                int currentY = currentPosition[1];
                
                // if current position is target, return steps
                if (currentX == TargetPos[0] && currentY == TargetPos[1]) {
                    return steps;
                }
                
                // explore all possible moves of the knight
                for (int[] direction : directions) {
                    int newX = currentX + direction[0];
                    int newY = currentY + direction[1];
                    
                    // check if new position is valid and not visited
                    if (newX >= 1 && newX <= N && newY >= 1 && newY <= N && !visited[newX][newY]) {
                        queue.offer(new int[]{newX, newY});
                        visited[newX][newY] = true;
                    }
                }
            }
            // increment steps after exploring all positions in current level
            steps++;
        }
        // if target position is not reachable, return -1
        return -1;
    }
}
